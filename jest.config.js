const {defaults} = require('jest-config');
module.exports = {
  moduleNameMapper: {
    '\\.(css|scss)$': '<rootDir>/config/CSSStub.js',
  },
};