import React from 'react';
import renderer from 'react-test-renderer';
import CompRoot from "./CompRoot.jsx";

test('Comp Root', () => {
  const component = renderer.create(
    <CompRoot />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});