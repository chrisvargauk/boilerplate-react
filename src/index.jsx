import React from 'react';
import ReactDOM from 'react-dom';
import CompRoot from "./comp/CompRoot.jsx";

ReactDOM.render(<CompRoot />, document.getElementById('root'));